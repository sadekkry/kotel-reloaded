#include "Controls.hpp"

Controls::Controls(const uint8_t btn, Encoder &enc) : btn(btn), enc(enc)
{
    pinMode(btn, INPUT_PULLUP);
    btn_val = false;
}

bool Controls::readButton()
{
    return digitalRead(btn);
}

bool Controls::readButtonEdge()
{
    if (digitalRead(btn) != btn_val)
    {
        btn_val = !btn_val;
        // falling edge
        if (!btn_val)
            return true;
    }
    return false;
}

int8_t Controls::readEncoder()
{
    int8_t enc_val = enc.read();
    enc.write(0);

    if (enc_val >= 4 || enc_val <= -4)
    {
        if (enc_val > 0)
            return ROTATE_RIGHT;
        else
            return ROTATE_LEFT;
    }
    return NO_ROTATE;
}