#include <Arduino.h>
#include <inttypes.h>
#include <Encoder.h>

class Controls
{
  private:
    const uint8_t btn;
    Encoder enc;

    bool btn_val;

  public:
    static const int8_t ROTATE_RIGHT = 1;
    static const int8_t NO_ROTATE = 0;
    static const int8_t ROTATE_LEFT = -1;

    Controls(const uint8_t btn, Encoder &enc);

    bool readButtonEdge();
    bool readButton();

    int8_t readEncoder();
};