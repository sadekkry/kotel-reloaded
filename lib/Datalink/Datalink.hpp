#include <Arduino.h>

class Datalink
{
private:
  const uint8_t pin;

public:
  Datalink(const uint8_t pin);
}