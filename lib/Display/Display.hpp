#include <Arduino.h>

class Display
{
  private:
    const uint8_t pin;

  public:
    Display(const uint8_t pin);
}