#include <Arduino.h>

class Pump
{
  private:
    const uint8_t pin;

  public:
    Pump(const uint8_t pin);
}