#include <Arduino.h>

class Statistics
{
private:
  const uint8_t pin;

public:
  Statistics(const uint8_t pin);
}