#include "Thermostat.hpp"

Thermostat::Thermostat(const uint8_t pin_a, const uint8_t pin_b, const uint8_t pin_c)
{
    pin[0] = pin_a;
    pin[1] = pin_b;
    pin[2] = pin_c;
    for (int i = 0; i < 3; ++i)
    {
        state[i] = 0;
        pinMode(pin[i], state[i]);
    }
}

void Thermostat::setTemp(const float temp)
{
    temp_set = temp;
}

void Thermostat::adjustTemp(const float temp)
{
    // TODO: regulation
}

const bool Thermostat::getState()
{
    return state[0] || state[1] || state[2];
}