#include <Arduino.h>

class Thermostat
{
  private:
    uint8_t pin[3];
    uint8_t state[3];

    float temp_set;

  public:
    Thermostat(const uint8_t pin_a, const uint8_t pin_b, const uint8_t pin_c);
    void setTemp(const float temp);
    void adjustTemp(const float temp);
    const bool getState();
};
