#include <Arduino.h>
#include <inttypes.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Encoder.h>

#include "Controls.hpp"
#include "Thermostat.hpp"
#include "Datalink.hpp"
#include "Pump.hpp"
#include "Display.hpp"
#include "Statistics.hpp"
#include "Rtc.hpp"
#include "Config.hpp"

// pins
#define P_ENCODERA 2
#define P_ENCODERB 3
#define P_ONEWIRE 4
#define P_FAN 5
#define P_BUZZER 6 // (depr.)
#define P_HEATER1 7
#define P_HEATER2 8
#define P_HEATER3 9
#define P_PUMP 10
#define P_LEDRED A0
#define P_WIFIRESET A1
#define P_LEDGREEN A2
#define P_BUTTON A3

// eeprom addrs.
#define E_VERSION 0    // Testovaci promenna prvniho spusteni
#define E_STATS 1      // Konfigurace chovani ridici jednotky
#define E_DSINNER 2    // Adresa vnitrniho teplomeru
#define E_DSOUTER 10   // Adresa vnejsiho teplomeru
#define E_TEMP 18      // Nastavena teplota
#define E_TEMPNIGHT 20 // Nastavena teplota pro nocni rezim
#define E_HOMEWIFI 22  // Je nastavena domaci Wi-Fi
#define E_SSIDLEN 23   // Delka domaci site
#define E_PWDLEN 24    // Delka hesla
#define E_SSID 25      // SSID domaci site (max 32)
#define E_PWD 57       // Heslo domaci site (max 64)
#define E_NMTIME 121   // Cas nocniho rezimu
#define E_SBMTIME 123

OneWire ow(P_ONEWIRE);
DallasTemperature temp(&ow);
Encoder e(P_ENCODERA, P_ENCODERB);

Controls controls(P_BUTTON, e);
Datalink link(Serial);
Thermostat thermo(P_HEATER1, P_HEATER2, P_HEATER3);
Pump pump(P_PUMP);
Statistics stats();
Timer timer();
Rtc rtc(ow);
Config cfg();

void setup()
{
    temp.begin();

    if (!controls.getButton())
    {
        delay(3000); // wait 3s
        if (!controls.getButton())
            config_loop();
    }
}

void loop()
{
    ctx_t ctx = cfg.getContext();
    // WIFI COMMUNICATION: =====================================================
    if (link)
    {
        switch (link.getMsgType())
        {
        case link.SET_TEMP:
            thermo.setTemp(link.getTemp());
            link.confirm();
        case link.GET_TEMP:
            link.sendTemp(thermo.getTemp());
        case link.GET_STATS:
            link.sendStats(stats.getStats(link.statsFrom(),
                                          link.statsTo()));
        case link.SET_TIME:
            rtc.setTime(link.getTime());
            link.confirm();
        case link.GET_TIME:
            link.setTime(rtc.getTime());
        default:
            link.error();
        }
    }

    // BUTTON PRESS: ===========================================================
    if (controls.getButtonEdge())
    {
        ctx = cfg.updateContext();
        display.change(ctx);
    }

    // ENCODER ROTATION: =======================================================
    int8_t enc = controls.getEncoder();
    if (ctx == cfg.TEMP)
    {
        float tmp = thermo.getTemp();
        tmp += enc;
        thermo.setTemp(tmp);
        display.showSetTemp(tmp);
    }
    else if (ctx == cfg.STATS)
    {
        cfg.updateInterval();
        display.showStats(ctx.statsInterval());
    }

    if (timer.quick())
    {
        // THERMOSTAT: =========================================================
        float tmp = temp.requestTempC(cfg.addr(cfg.TEMP_WATER));

        // turn off when thermometer is not responding
        if (tmp == DEVICE_DISCONNECTED_C)
        {
            cfg.logError();     // count numer of failed temp read attempts
            if (cfg.getError()) // if count > treshold -> block program
            {
                display.showError();
                digitalWrite(P_LEDGREEN, 0);
                thermo.stop();
                pump.run();
                while (true)
                {
                    digitalWrite(P_LEDRED, !digitalRead(P_LEDRED));
                    delay(200);
                }
            }
            continue;
        }
        else
            cfg.noError();
        thermo.adjustTemp(tmp);

        // DISPLAY TEMP: =======================================================
        if (ctx == cfg.TEMP)
            display.showTemp(tmp);

        // LED: ================================================================
        digitalWrite(P_LEDRED, thermo.state());

        // COOLING: ============================================================
        tmp = temp.requestTempC(cfg.addr(cfg.TEMP_MCU));
        if (tmp == DEVICE_DISCONNECTED_C)
            cool.run(); // turn on cooling if thermometer is not working
        else
            cool.adjustSpeed(tmp);

        // PUMP: ===============================================================
        if (thermo.state() || thermo.getTemp() > TEMP_PUMP_LIMIT)
            pump.run();
        else
            pump.stop();
    }

    // POWER CONSUMPTION: ======================================================
    if (timer.slow())
    {
        rtc_t time_current = rtc.getTime();

        stats.update(time_current, thermo.getState());

        // DISPLAY TIME: =======================================================
        if (ctx == cfg.RTC)
            display.showTime(time_current);
        else if (ctx == cfg.STATS)
            display.showStats(ctx.statsInterval(),
                              stats.getStats(ctx.statsSum(), time_current));
    }
}

void config_loop()
{
    // listen for serial, config some basic stuff
}